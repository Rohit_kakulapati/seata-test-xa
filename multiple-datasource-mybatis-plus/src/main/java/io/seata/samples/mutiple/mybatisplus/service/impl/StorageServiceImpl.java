package io.seata.samples.mutiple.mybatisplus.service.impl;

import io.seata.core.context.RootContext;
import io.seata.samples.mutiple.mybatisplus.common.storage.Product;
import io.seata.samples.mutiple.mybatisplus.config.DataSourceKey;
import io.seata.samples.mutiple.mybatisplus.config.DynamicDataSourceContextHolder;
import io.seata.samples.mutiple.mybatisplus.dao.ProductDao;
import io.seata.samples.mutiple.mybatisplus.service.StorageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author HelloWoodes
 */
@Service
@Slf4j
public class StorageServiceImpl implements StorageService {

    @Autowired
    private ProductDao productDao;

    /**
     * 事务传播特性设置为 REQUIRES_NEW 开启新的事务
     *
     * @param productId 商品 ID
     * @param amount    扣减数量
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
    @Override
    public boolean reduceStock(Long productId, Integer amount) throws Exception {
        log.info("=============STORAGE=================");
        DynamicDataSourceContextHolder.setDataSourceKey(DataSourceKey.STORAGE);
        log.info("setting data source key for storage XID: {}", RootContext.getXID());

        // 检查库存
        checkStock(productId, amount);

        log.info("checking stock for productId: {}", productId);
        // 扣减库存
        Product product = productDao.selectById(productId);
        product.setStock(product.getStock() - amount);
        Integer record = productDao.updateById(product);
        log.info("update by id done for product id {} status:{}", productId, record > 0 ? "success" : "failure");

        return record > 0;

    }

    private void checkStock(Long productId, Integer requiredAmount) throws Exception {

        log.info("checking stock for productId {} ", productId);
        Product product = productDao.selectById(productId);

        if (product.getStock() < requiredAmount) {
            log.warn("for productId {} the required amount {} is greater than :{}", productId, requiredAmount,product.getStock());
            throw new Exception("库存不足");
        }
    }
}
