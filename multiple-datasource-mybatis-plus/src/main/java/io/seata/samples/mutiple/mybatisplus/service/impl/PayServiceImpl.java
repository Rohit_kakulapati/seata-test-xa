package io.seata.samples.mutiple.mybatisplus.service.impl;

import io.seata.core.context.RootContext;
import io.seata.samples.mutiple.mybatisplus.common.pay.Account;
import io.seata.samples.mutiple.mybatisplus.config.DataSourceKey;
import io.seata.samples.mutiple.mybatisplus.config.DynamicDataSourceContextHolder;
import io.seata.samples.mutiple.mybatisplus.dao.AccountDao;
import io.seata.samples.mutiple.mybatisplus.service.PayService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author HelloWoodes
 */
@Service
@Slf4j
public class PayServiceImpl implements PayService {

    @Autowired
    private AccountDao accountDao;

    /**
     * 事务传播特性设置为 REQUIRES_NEW 开启新的事务
     *
     * @param userId 用户 ID
     * @param price  扣减金额
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
    @Override
    public boolean reduceBalance(Long userId, Integer price) throws Exception {
        log.info("=============PAY=================");
        DynamicDataSourceContextHolder.setDataSourceKey(DataSourceKey.PAY);
        log.info("added data source key PAY XID: {}", RootContext.getXID());

        checkBalance(userId, price);

        log.info("selecting account by userId {}", userId);
        Account account = accountDao.selectById(userId);

        account.setBalance(account.getBalance() - price);
        Integer record = accountDao.updateById(account);
        log.info("updating account for user {} status:{}", userId, record > 0 ? "success" : "failure");

        return record > 0;

    }

    private void checkBalance(Long userId, Integer price) throws Exception {
        log.info("checking balance for user {} ", userId);
        Account account = accountDao.selectById(userId);

        Integer balance = account.getBalance();

        if (balance < price) {
            log.warn("user :{} has less balance :{} than the price : {}", userId, balance, price);
            throw new Exception("余额不足");
        }

    }

}

